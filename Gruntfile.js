module.exports = function (grunt) {
  'use strict';

  var _ = require('lodash');
  var path = require('path');

  // Load all npm dependencies
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bower: grunt.file.readJSON('bower.json'),


    //
    // Configure projects validators
    // -----------------------------------------

    jshint: {
      dev: {
        options: {
          globals: {
            "console": true,
            "require": true,
            "define": true,
            "_": true,
            "$": true,
            "Backbone": true
          }
        },
        src: [
          'app/scripts/*.js',
          'app/widgets/**/*.js'
        ]
      }
    },

    csslint: {
      dev: {
        options: {
          "adjoining-classes": false,
          "box-sizing": false,
          "box-model": false,
          "compatible-vendor-prefixes": false,
          "floats": false,
          "font-sizes": false,
          "gradients": false,
          "important": false,
          "known-properties": false,
          "outline-none": false,
          "qualified-headings": false,
          "regex-selectors": false,
          "shorthand": false,
          "text-indent": false,
          "unique-headings": false,
          "universal-selector": false,
          "unqualified-attributes": false
        },
        src: ['app/styles/*.css']
      }
    },


    //
    // Configure watching project's development
    // -----------------------------------------

    express: {
      dev: {
        options: {
          port: 9000,
          hostname: 'localhost',
          bases: ['app'],
          livereload: true
        }
      }
    },

    open: {
      dev: {
        url: 'http://localhost:<%= express.dev.options.port %>'
      }
    },

    watch: {
      less: {
        files: 'app/styles/*.less',
        tasks: ['less:dev']
      },
      css: {
        files: 'app/styles/application.css',
        tasks: ['csslint:dev'],
        options: {
          livereload: true
        }
      },
      js: {
        files: [
          'app/scripts/*.js',
          'app/widgets/**/*.js'
        ],
        tasks: ['jshint:dev'],
        options: {
          livereload: true
        }
      }
    },


    //
    // Configure installing project's files
    // -----------------------------------------

    clean: {
      dev: ['app/libs/*'],
      dist: ['dist/*']
    },

    copy: {
      dist: {
        files: [
          {expand: true, src: ['app/fonts/*'], dest: 'dist/fonts/'}
        ]
      }
    },


    //
    // Configure creating documentation files
    // -----------------------------------------

    yuidoc: {
      compile: {
        name: "<%= pkg.name %>",
        description: "<%= pkg.description %>",
        version: "<%= pkg.version %>",
        url: "<%= pkg.homepage %>",
        options: {
          paths: [ "app/scripts", "app/widgets" ],
          outdir: "docs"
        }
      }
    },


    //
    // Configure building project's files
    // -----------------------------------------

    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          removeCommentsFromCDATA: true,
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: false,
          removeRedundantAttributes: false,
          useShortDoctype: true,
          removeEmptyAttributes: false,
          removeOptionalTags: false
        },
        files: [{
          expand: true,
          cwd: 'app',
          src: '*.html',
          dest: 'dist'
        }]
      }
    },

    less: {
      dev: {
        options: {
          paths: ['app/styles']
        },
        files: {
          'app/styles/application.css': 'app/styles/application.less'
        }
      },
      dist: {
        options: {
          paths: ['app/styles'],
          yuicompress: true
        },
        files: {
          'dist/styles/application.css': 'app/styles/application.less'
        }
      }
    },

    imagemin: {
      dist: {
        files: [
          {expand: true, cwd: 'app/images', src: '*.{png,jpg,jpeg,gif}', dest: 'dist/images'}
        ]
      }
    },

    requirejs: {
      dist: {
        options: {
          baseUrl: 'app',
          mainConfigFile: 'app/scripts/application.js',
          findNestedDependencies: true,
          removeCombined: false,
          optimize: 'uglify',
          include: ['scripts/application'],
          exclude: ['jquery', 'lodash', 'text', 'backbone', 'emitter'],
          out: 'dist/scripts/application.js'
        }
      }
    },

    uglify: {
      dist: {
        options: {
          preserveComments: 'some'
        },
        files: [
          {expand: true, src: '*.js', dest: 'dist/libs', cwd: 'app/libs'},
        ]
      }
    }

  });


  //
  // Register all important tasks
  // -----------------------------------------

  // Install bower components inside our project
  grunt.registerTask('libs', 'Copy bower components', function() {
    var bower = grunt.config.get('bower'), status = true;
    _.each(bower.copy, function(dist, src) {
      var files = grunt.file.expand('bower_components/' + src);
      
      _.each(files, function(file) {
        var destination = 'app/' + dist;
        if (path.extname(dist) === '') {
          destination = 'app/' + dist + '/' + path.basename(file);
        }
        grunt.file.copy(file, destination);
        grunt.log.writeln('Copy success: ' + destination);
      });

      if (files.length === 0) {
        grunt.log.error('Path does\'t exists: ' + src);
        status = false;
      }
    });
    return status;
  });

  // I'm pretty out of habit to use this
  grunt.registerTask('lint', ['jshint', 'less:dev', 'csslint']);

  // Open server and listen to all changes
  grunt.registerTask('run', ['less:dev', 'express', 'open', 'watch']);

  // Time to deploy
  grunt.registerTask('build', ['lint', 'clean:dist', 'htmlmin:dist', 'less:dist', 'imagemin:dist', 'copy:dist', 'requirejs:dist', 'uglify:dist']);

  // For now as default check javascripts
  grunt.registerTask('default', ['lint']);

};
