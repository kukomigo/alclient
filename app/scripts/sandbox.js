define([
  'underscore',
  'jquery',
  'backbone',
  'emitter',
  'storage'
],

function(_, $, Backbone, EventEmitter) {

  // Initialize EventEmmiter class.
  var emitter = new EventEmitter();
  emitter.setMaxListeners(0);

  // Route the initial URL.
  Backbone.history.start({ pushState: true });

  /**
   * Attach listeners to our sandbox so we can use this context inside our
   * callback. Connects both on and once listeners to EventEmitter.
   *
   * @private
   * @method attachListener
   * @param  {String} listenerType
   * @return {Function}
   */
  var attachListener = function(listenerType) {
    return function (name, listener, context) {
      if (!_.isFunction(listener) || !_.isString(name)) {
        console.error('Invalid arguments passed to sandbox.' + listenerType);
      }
      context = context || this;
      var callback = function() {
        var args = Array.prototype.slice.call(arguments);
        try {
          listener.apply(context, args);
        } catch(e) {
          console.error("Error caught in listener '" + name + "', called with arguments: ", args, "\nError:", e.message, e, args);
        }
      };
      emitter[listenerType](name, callback);
    };
  };

  /**
   * The App's sandbox class.
   *
   * @public
   * @class sandbox
   * @constructor
   */
  var sandbox = {};

  /**
   * The sandbox's config object.
   *
   * @property config
   * @type {Object}
   * @default {}
   */
  sandbox.config = {};

  /**
   * The sandbox's alias for lodash extend method.
   *
   * @param  {Object} [args] Objects to be combined
   * @return {Object}
   */
  sandbox.extend = _.extend;

  /**
   * The sandbox's alias for Backbone.Model class.
   * See: http://backbonejs.org/#Model
   *
   * Models are the heart of any JavaScript application, containing the
   * interactive data as well as a large part of the logic surrounding it:
   * conversions, validations, computed properties, and access control.
   *
   * @method Model
   * @param  {Object} object
   * @return {Object}
   */
  sandbox.Model = function(object) {
    return Backbone.Model.extend(object);
  };

  /**
   * The sandbox's alias for Backbone.View class.
   * See: http://backbonejs.org/#View
   *
   * Views organize your interface into logical views, backed by models.
   *
   * @method View
   * @param  {Object} object
   * @return {Object}
   */
  sandbox.View = function(object) {
    return Backbone.View.extend(object);
  };

  /**
   * The sandbox's alias for Backbone.Collection class.
   * See: http://backbonejs.org/#Collection
   *
   * Collections are ordered sets of models.
   *
   * @method Collection
   * @param  {Object} object
   * @return {Object}
   */
  sandbox.Collection = function(object) {
    return Backbone.Collection.extend(object);
  };

  /**
   * The sandbox's alias for Backbone.Router class.
   * See: http://backbonejs.org/#Router
   *
   * Router provides methods for routing client-side pages, and connecting
   * them to actions and events
   *
   * @method Router
   * @param  {Object} object
   * @return {Object}
   */
  sandbox.Router = Backbone.Router;

  /**
   * The sandbox's alias for Backbone.Router class.
   * See: http://backbonejs.org/#Router
   *
   * Router provides methods for routing client-side pages, and connecting
   * them to actions and events
   *
   * @method Router
   * @param  {Object} object
   * @return {Object}
   */
  sandbox.Store = Backbone.LocalStorage;

  /**
   * The sandbox's alias for underscore template library.
   *
   * @method template
   * @param  {Object} obj The model
   * @return {Function} Results in underscore template parser
   */
  sandbox.template = _.template;

  /**
   * Attach listener EventEmitter.on to our sandbox.
   *
   * @method on
   * @param {String} name
   * @param {String} listener
   * @param {Object} context
   */
  sandbox.on = attachListener('on');

  /**
   * Attach listener EventEmitter.once to our sandbox.
   *
   * @method once
   * @param {String} name
   * @param {String} listener
   * @param {Object} context
   */
  sandbox.once = attachListener('once');

  /**
   * Attach EventEmitter.emit method to our sandbox.
   *
   * @method emit
   * @param {String} name  Event name to emit
   * @param {mixed} args   Passed arguments
   */
  sandbox.emit = function() {
    var args = Array.prototype.slice.call(arguments);
    emitter.emit.apply(emitter, args);
  };

  /**
   * Attach EventEmitter.off method to our sandbox.
   *
   * @method off
   * @param {String} name  Event name to quit
   * @param {mixed} args   Passed arguments
   */
  sandbox.off = function() {
    var args = Array.prototype.slice.call(arguments);
    emitter.off.apply(emitter, args);
  };

  /**
   * GUID generator.
   *
   * @method guid
   * @return {String} Random guid
   */
  sandbox.guid = function() {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
  };

  /**
   * Find a specific widget element.
   *
   * @method widget
   * @param  {String} name
   * @return {Object} Results in jQuery DOM object
   */
  sandbox.widget = function(name) {
    return $('[data-widget="' + name + '"]');
  };

  return sandbox;
});
