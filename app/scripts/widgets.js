define([
  'underscore'
],

function(_) {

  /**
   * Run specific widget.
   *
   * @private
   * @method execute
   * @param {Function} widget
   */
  function initWidget(channel, options) {
    require(["widgets/" + channel + "/main"], function(widget) {
      try {
        widget(options);
      } catch(e) {
        console.error(e);
      }
    });
  }

  /**
   * The App's widgets manager.
   *
   * @public
   * @class widgets
   * @constructor
   */
  var widgets = {};

  /**
   * The storage of all loaded widgets.
   *
   * @property channels
   * @type {Object}
   * @default {}
   */
  widgets.channels = {};

  /**
   * Automatically load a widget and initialize it.
   *
   * @method start
   * @param {String|Object} arguments
   */
  widgets.start = function() {
    var args = Array.prototype.slice.call(arguments);

    _.each(args, function(arg) {
      var channel, options;

      // Collect data from arguments.
      if (_.isString(arg)) {
        channel = arg;
        options = { el: arg };
      } else if (_.isObject(arg)) {
        channel = arg.channel;
        options = arg.options || { el: channel };
      }

      // Register only new channels.
      if (channel && this.channels[channel] === undefined) {
        this.channels[channel] = options;
        initWidget(channel, options);
      }
    }, this);
  };

  return widgets;

});
