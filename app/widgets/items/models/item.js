define([
  'sandbox'
],

function(sandbox) {
  var ItemModel = sandbox.Model({

    // Default attributes for the todo.
    defaults: {
      id: '',
      time: 0,
      title: '',
      description: 'Sample description',
      tags: 'default',
      completed: 0
    },

    /**
     * Defines which and how our data will be presented when edit mode
     * is turned off. All settings here are translated by details widged.
     *
     * @public
     * @method getPresentation
     * @returns {Object}
     */
    getPresentation: function() {
      return {
        time: {
          value: this.get('time'),
          type: 'text',
          css: ['date']
        },
        title: {
          value: this.get('title'),
          type: 'text',
          css: ['title']
        },
        id: {
          value: this.get('id'),
          type: 'text',
          css: ['identifier']
        },
        description: {
          value: this.get('description'),
          type: 'text'
        }
      };
    },

    /**
     * Defines which and how our data will be presented when edit mode
     * is turned on. All settings here are translated by details widged.
     *
     * @public
     * @method getPublisher
     * @returns {Object}
     */
    getPublisher: function() {
      return {
        title: {
          value: this.get('title'),
          type: 'input'
        },
        description: {
          value: this.get('description'),
          type: 'textarea'
        },
        completed: {
          value: this.get('completed'),
          type: 'select',
          options: {
            '0': 'No',
            '1': 'Yes'
          },
          parseInt: true
        },
        tags: {
          value: this.get('tags'),
          type: 'input'
        }
      };
    }

  });

  return ItemModel;
});
