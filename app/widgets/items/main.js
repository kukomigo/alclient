define([
  'sandbox',
  'widgets/items/views/widget'
],

function(sandbox, WidgetView) {
  return function(options) {
    new WidgetView({
      el: sandbox.widget(options.el)
    });
  };
});
