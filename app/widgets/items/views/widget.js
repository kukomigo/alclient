define([
  'sandbox',
  'widgets/items/collections/items',
  'widgets/items/views/item',
  'text!widgets/items/templates/box.html'
],

function(sandbox, items, ItemView, boxTemplate) {
  var WidgetView = sandbox.View({

    template: sandbox.template(boxTemplate),

    initialize: function () {
      items.fetch();
      this.render();
      sandbox.on('item:selected', this.deselectOldItem, this);
      sandbox.on('item:add', this.addItem, this);
    },

    /**
     * Renders widgets box, where all items are stored. Gets all items from
     * colection and renders them inside that box.
     *
     * @method render
     */
    render: function() {
      this.$el.html(this.template);
      items.each(this.renderItem, this);
    },

    /**
     * Renders an item based on a passed model data.
     *
     * @method renderItem
     * @param {Object} model Item's model
     */
    renderItem: function(model) {
      var view = new ItemView({ model: model });
      this.$el.find('.items').prepend(view.render().el);
    },

    /**
     * Adds new item to a collection and renders it on list.
     *
     * @method addItem
     * @param {Object} model Newly created item's model
     */
    addItem: function(model) {
      var item;

      // Generete GUID for new item and set its creation date.
      model.id = sandbox.guid();
      model.time = new Date().getTime();

      // Add created item to a collection.
      item = items.add(model);

      // Render newly added item.
      this.renderItem(item);
    },

    /**
     * Deselects previously selected item when new item is chosen.
     *
     * @method deselectOldItem
     * @param {Object} model Currently selected item's model
     */
    deselectOldItem: function(model) {
      var previous = this.$el.find('.active').not('[data-cid="' + model.cid + '"]');
      if (previous.length === 1) {
        items.get(previous.data('cid')).trigger('deselect');
      }
    }

  });

  return WidgetView;
});
