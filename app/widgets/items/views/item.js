define([
  'sandbox',
  'text!widgets/items/templates/item.html'
],

function(sandbox, itemTemplate) {
  var ItemView = sandbox.View({

    // Our template for the element.
    template: sandbox.template(itemTemplate),

    // Each newly created element will be put inside this tag.
    tagName: 'div',
    className: 'item',

    // The DOM events specific to an item.
    events: {
      'click': 'select',
      'click .remove': 'destroy'
    },

    // Listens to changes inside our model.
    initialize: function () {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
      this.model.bind('deselect', this.deselect, this);
    },

    // Render the contents of the item.
    render: function() {
      var attributes = this.model.attributes;
      this.$el.attr('data-cid', this.model.cid);
      this.$el.html(this.template(attributes));
      this.model.save();
      return this;
    },

    // Perform select action.
    select: function(e) {
      if (e.which === 1) {
        this.$el.addClass('active');
        sandbox.emit('item:selected', this.model);
      }
    },

    // Perform deselect action
    deselect: function() {
      this.$el.removeClass('active');
    },

    destroy: function() {
      this.model.destroy();
    }

  });

  return ItemView;
});
