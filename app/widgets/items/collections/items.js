define([
  'sandbox',
  'widgets/items/models/item'
],

function(sandbox, ItemModel) {
  var ItemsCollection = sandbox.Collection({

    url: 'api/items',
    localStorage: new sandbox.Store('lokal_api_items'),

    // Reference to this collection's model.
    model: ItemModel,

    comparator: function(model) {
      return model.get('time');
    }

  });

  return new ItemsCollection();
});
