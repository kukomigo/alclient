define([
  'sandbox',
  'widgets/actions/views/widget'
],

function(sandbox, WidgetView) {
  return function(options) {
    new WidgetView({
      el: sandbox.widget(options.el)
    });
  };
});
