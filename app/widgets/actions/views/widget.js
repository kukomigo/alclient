define([
  'sandbox',
  'text!widgets/actions/templates/box.html'
],

function(sandbox, boxTemplate) {
  var WidgetView = sandbox.View({

    template: sandbox.template(boxTemplate),

    events: {
      'keypress .add-item': 'requestAdd',
      'change .switch-edit': 'requestSwitch'
    },

    initialize: function () {
      this.render();
    },

    render: function() {
      this.$el.html(this.template);
    },

    emitAddItem: function() {
      var result = {
        title: this.$el.find('input').val()
      };
      sandbox.emit('item:add', result);
      this.$el.find('input').val('');
    },

    requestAdd: function(e) {
      if (e.keyCode == 13) {
        this.emitAddItem();
      }
    },

    requestSwitch: function(e) {
      var status = this.$(e.target).is(':checked');
      sandbox.config.editMode = status;
      sandbox.emit('actions:mode-switched', status);
    }

  });

  return WidgetView;
});
