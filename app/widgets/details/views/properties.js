define([
  'widgets/details/components/text',
  'widgets/details/components/input',
  'widgets/details/components/select',
  'widgets/details/components/textarea'
],

function(TextView, InputView, SelectView, TextareaView) {

  var viewMapping = {
    'text': TextView,
    'input': InputView,
    'select': SelectView,
    'textarea': TextareaView
  };

  return function(type) {
    if (type !== undefined && viewMapping[type] !== undefined) {
      return viewMapping[type];
    } else {
      return viewMapping.input;
    }
  };

});
