define([
  'sandbox'
],

function(sandbox) {
  var PropertyView = sandbox.View({

    template: sandbox.template('<label><%- name %></label>'),

    tagName: 'div',
    className: 'property',

    initialize: function(options) {
      this.property = options.property;
    },

    render: function() {
      this.$el.html(this.template(this.property));
      return this;
    }

  });

  return PropertyView;
});
