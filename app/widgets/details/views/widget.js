define([
  'sandbox',
  'underscore',
  'widgets/details/views/properties'
],

function(sandbox, _, propertyView) {
  var WidgetView = sandbox.View({

    model: {},

    initialize: function () {
      sandbox.on('item:selected', this.itemSelected, this);
      sandbox.on('actions:mode-switched', this.render, this);
    },

    /**
     * Render view each time user selects new item.
     *
     * @method itemSelected
     * @param {Object} model
     */
    itemSelected: function(model) {
      if (this.model.cid !== model.cid) {
        this.model = model;
        this.render();
      }
    },

    /**
     * Renders widget view. Based on the mode displays presentation or
     * edit content.
     *
     * @method render
     */
    render: function() {
      var properties;

      // Ensure we have data.
      if (this.model.attributes) {
        this.$el.html('<div class="details"></div>');

        // Show different view when edit mode is turned on.
        if (sandbox.config.editMode) {

          // Get publisher's properties and render them.
          properties = this.model.getPublisher();
          _.each(properties, this.renderProperty, this);

        } else {

          // Get presentation's properties and render them.
          properties = this.model.getPresentation();
          _.each(properties, this.renderProperty, this);

        }
      }
    },

    /**
     * Renders individual property inside widget when edit mode is turned on.
     *
     * @method renderProperty
     * @param {Object} property
     * @param {String} name
     */
    renderProperty: function(property, name) {
      var view;

      // Combine property name into one object.
      property.name = name;

      // Load proper view, based on property type.
      view = new (propertyView(property.type))({
        model: this.model,
        property: property
      });

      // Append property to widget view.
      this.$('.details').append(view.render().el);
    }

  });

  return WidgetView;
});
