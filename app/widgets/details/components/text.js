define([
  'sandbox',
  'underscore',
  'widgets/details/views/property',
  'text!widgets/details/templates/text.html'
],

function(sandbox, _, Property, template) {
  var TextView = Property.extend({

    template: sandbox.template(template),

    className: 'static',

    initialize: function(options) {
      this.property = options.property;
      this.preprocess();
    },

    preprocess: function() {
      var css = this.property.css;

      // Generate html class for current property.
      if (css && _.isArray(css)) {
        this.property.css = ' class="' + css.join(' ') + '"';
      } else if (css && _.isString(css)) {
        this.property.css = ' class="' + css + '"';
      } else {
        this.property.css = '';
      }
    }

  });

  return TextView;
});
