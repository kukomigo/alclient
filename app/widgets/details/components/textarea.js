define([
  'sandbox',
  'widgets/details/views/property',
  'text!widgets/details/templates/textarea.html'
],

function(sandbox, Property, template) {
  var TextareaView = Property.extend({

    template: sandbox.template(template),

    events: {
      'change textarea': 'updateModel',
      'keyup textarea': 'updateModel'
    },

    updateModel: function() {
      var value = this.$el.find(':input').val();
      this.model.set(this.property.name, value);
    }

  });

  return TextareaView;
});
