define([
  'sandbox',
  'widgets/details/views/property',
  'text!widgets/details/templates/select.html',
],

function(sandbox, Property, template) {
  var SelectView = Property.extend({

    template: sandbox.template(template),

    events: {
      'change select': 'updateModel'
    },

    updateModel: function(e) {
      var value = this.$(e.target).val();
      if (this.property.parseInt) {
        value = parseInt(value, 10);
      }
      this.model.set(this.property.name, value);
    }

  });

  return SelectView;
});
