define([
  'sandbox',
  'widgets/details/views/property',
  'text!widgets/details/templates/input.html',
],

function(sandbox, Property, template) {
  var InputView = Property.extend({

    template: sandbox.template(template),

    events: {
      'change input': 'updateModel',
      'keyup input': 'updateModel'
    },

    updateModel: function() {
      var value = this.$el.find('input').val();
      this.model.set(this.property.name, value);
    }

  });

  return InputView;
});
